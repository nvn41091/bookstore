package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SachTemp {
    private Integer id;
    private TacGia tacGia;
    private String loaiSach;
    private String tenSach;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date xuatBan;
    private String nhaXuatBan;
    private String nhaPhatHanh;
    private String dangBia;
    private String kichThuoc;
    private String khoiLuong;
    private Integer soTrang;
    private float rate;
    private int gia;
    private String anh;
    private String gioiThieu;
    private MultipartFile anhTemp;


}
