package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class Comment {

    private int id;
    private User user;
    private String noiDung;
    private Date ngayComment;

}
