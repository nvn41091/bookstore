package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {

    private Integer id;
    private String username;
    private String password;
    private String hoTen;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ngaySinh;
    private Integer gioiTinh;
    private String diaChi;
    private String soDienThoai;
    private String quyen;
    private int enable;

}
