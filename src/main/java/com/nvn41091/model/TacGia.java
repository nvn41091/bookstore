package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TacGia {

    private Integer id;
    private String tenTacGia;
    private Date ngaySinh;
    private String tieuSu;

}
