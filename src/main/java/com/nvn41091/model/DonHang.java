package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class DonHang {

    private int id;
    private int idUser;
    private int trangThai;
    private Date ngayDatHang;
    private Date ngayGiaoHang;
    private String noiGiaoHang;
    private String soDienThoai;
    private long tongTien;
    private String ho;
    private String ten;
    private String email;
    private List<ChiTietDonHang> listCTDonHang;

}
