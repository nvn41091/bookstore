package com.nvn41091.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@ToString
@NoArgsConstructor
public class ChiTietDonHang {

    private int id;
    private int giaBan;
    private int soLuong;
    private Sach sach;

}
