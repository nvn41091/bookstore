package com.nvn41091.controller.home;

import com.nvn41091.model.Sach;
import com.nvn41091.service.SachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/sach")
public class SachController {

    @Autowired
    private SachService sachService;

    @GetMapping("/{id}")
    public String getSingleProduct(@PathVariable int id,
                                   ModelMap map){
        Sach sach = sachService.getBookById(id);
        map.addAttribute("book", sach);
        map.addAttribute("tacGia", sachService.getBookByTacGia(sach.getTacGia().getTenTacGia(), 1));
        map.addAttribute("loaiSach", sachService.getBookByLoaiSach(sach.getLoaiSach(), 1));
        return "other/single-product";
    }

}
