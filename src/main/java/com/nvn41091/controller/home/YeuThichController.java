package com.nvn41091.controller.home;

import com.nvn41091.model.Sach;
import com.nvn41091.model.User;
import com.nvn41091.model.YeuThich;
import com.nvn41091.service.SachService;
import com.nvn41091.service.UserService;
import com.nvn41091.service.YeuThichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;

@Controller
public class YeuThichController {

    @Autowired
    YeuThichService yeuThichService;

    @Autowired
    SachService sachService;

    @Autowired
    UserService userService;

    @GetMapping("/them-yeu-thich/{id}")
    public String themYeuThich(@PathVariable("id") int id , ModelMap map){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        User user = null;
        try{
            user = userService.getUserByUsername(((UserDetails) principal).getUsername());
        } catch (Exception e){
        }
        if (user != null) {
            yeuThichService.addYeuThich(user.getId(), id);
            List<YeuThich> listId = yeuThichService.getListYeuThich(user.getId());
            List<Sach> list = new ArrayList<>();
            for ( YeuThich yeuThich : listId){
                list.add(sachService.getBookById(yeuThich.getIdSach()));
            }
            map.addAttribute("wishlist", list);
        } else {
            map.addAttribute("error", "Bạn cần đăng nhập để tiếp tục");
        }
        return "other/wishlist";
    }

    @GetMapping("/xoa-yeu-thich/{id}")
    public String xoaYeuThich(@PathVariable("id") int id, ModelMap map){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;
        try{
            user = userService.getUserByUsername(((UserDetails) principal).getUsername());
        } catch (Exception e){
        }
        if (user != null) {
            yeuThichService.deleteYeuThich(user.getId(), id);
            List<YeuThich> listId = yeuThichService.getListYeuThich(user.getId());
            List<Sach> list = new ArrayList<>();
            for ( YeuThich yeuThich : listId){
                list.add(sachService.getBookById(yeuThich.getIdSach()));
            }
            map.addAttribute("wishlist", list);
        } else {
            map.addAttribute("error", "Bạn cần đăng nhập để tiếp tục");
        }
        return "other/wishlist";
    }
}
