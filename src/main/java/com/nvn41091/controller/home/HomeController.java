package com.nvn41091.controller.home;

import com.nvn41091.dao.ChiTietDonHangDAO;
import com.nvn41091.model.ChiTietDonHang;
import com.nvn41091.model.Sach;
import com.nvn41091.model.User;
import com.nvn41091.model.YeuThich;
import com.nvn41091.service.SachService;
import com.nvn41091.service.UserService;
import com.nvn41091.service.YeuThichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    UserService userService;

    @Autowired
    private SachService sachService;

    @Autowired
    private YeuThichService yeuThichService;

    @Autowired
    private ChiTietDonHangDAO chiTietDonHangDAO;

    @GetMapping("/login")
    public String loginController(
            @RequestParam(required = false, name = "error") String error, ModelMap map) {
        if (error != null) {
            map.addAttribute("message", "Username hoặc mật khẩu không hợp lệ");
        }
        return "other/login";
    }

    @GetMapping("/")
    public String homeController(ModelMap map) {
        map.addAttribute("newBook", sachService.getNewBook());
        map.addAttribute("buyest", sachService.getBookByBuy());
        return "index";
    }

    @GetMapping("/forward-user")
    public String forwardUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserDetails userDetails = (UserDetails) principal;
        List<GrantedAuthority> list = (List<GrantedAuthority>) userDetails.getAuthorities();
        for (GrantedAuthority grantedAuthority : list) {
            String quyen = grantedAuthority.getAuthority();
            if (quyen.equals("ROLE_ADMIN")) {
                return "redirect:/admin/";
            } else {
                return "redirect:/";
            }
        }
        return "redirect:/";
    }

    @PostMapping("/register")
    public String addUser(@ModelAttribute("user") User user) {
        try {
            userService.addUser(user);
        } catch (Exception e) {
        }
        return "redirect:/login";
    }

    @GetMapping("/xem-hang")
    public String xemHang(ModelMap map){
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserDetails userDetails = (UserDetails) principal;
            User user = userService.getUserByUsername(userDetails.getUsername());
            List<ChiTietDonHang> chiTietDonHang = chiTietDonHangDAO.getListByUser(user.getId());
            map.addAttribute("cart", chiTietDonHang);
        } catch (Exception e){
        }
        return "other/xemHang";
    }

    @GetMapping("/yeu-thich")
    public String wishList(ModelMap map) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = null;
        try {
            user = userService.getUserByUsername(((UserDetails) principal).getUsername());
        } catch (Exception e) {
        }
        if (user != null) {
            List<YeuThich> listId = yeuThichService.getListYeuThich(user.getId());
            List<Sach> list = new ArrayList<>();
            for (YeuThich yeuThich : listId) {
                list.add(sachService.getBookById(yeuThich.getIdSach()));
            }
            map.addAttribute("wishlist", list);
        } else {
            map.addAttribute("error", "Bạn cần đăng nhập để tiếp tục");
        }
        return "other/wishlist";
    }

    @GetMapping("/tim-kiem")
    public String timKiem(@RequestParam("ten") String s, ModelMap map) {
        List<Sach> list = sachService.searchSach(s);
        int size = 0;
        if (list != null) {
            size = list.size();
        }
        map.addAttribute("items", list);
        map.addAttribute("size", size);
        return "other/all-shop";
    }

    @GetMapping("/loai-sach/{id}")
    public String getLoaiSach(@PathVariable("id") int id, ModelMap map) {
        List<Sach> list = sachService.getListByLoaiSach(id);
        int size = 0;
        if (list != null) {
            size = list.size();
        }
        map.addAttribute("items", list);
        map.addAttribute("size", size);
        return "other/all-shop";
    }

    @GetMapping("/404")
    public String error404() {
        return "other/404";
    }

    @GetMapping("/lien-he")
    public String lienHe() {
        return "other/contact";
    }

    @GetMapping("/cau-hoi")
    public String cauHoi() {
        return "other/faq";
    }

    @GetMapping("/dang-ki")
    public String dangKi(ModelMap map) {
        User user = new User();
        user.setNgaySinh(new Date(System.currentTimeMillis()));
        map.addAttribute("user", user);
        return "other/register";
    }

    @GetMapping("/gioi-thieu")
    public String gioiThieu() {
        return "other/about";
    }

}
