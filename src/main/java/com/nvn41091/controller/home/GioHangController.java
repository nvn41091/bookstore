package com.nvn41091.controller.home;

import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.ChiTietDonHang;
import com.nvn41091.model.DonHang;
import com.nvn41091.model.Sach;
import com.nvn41091.model.User;
import com.nvn41091.service.DonHangService;
import com.nvn41091.service.SachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes(value = {"cart", "tinhTien"})
public class GioHangController {

    @Autowired
    SachService sachService;

    @Autowired
    UserDAO userDAO;

    @Autowired
    DonHangService donHangService;

    @GetMapping("/them/{id}")
    public String themGioHang(@PathVariable int id,
                              ModelMap map){
        Sach sach = sachService.getBookById(id);
        if (map.getAttribute("cart") == null){
            ChiTietDonHang chiTietDonHang = new ChiTietDonHang();
            chiTietDonHang.setSoLuong(1);
            chiTietDonHang.setSach(sach);
            chiTietDonHang.setGiaBan(sach.getGia());

            List<ChiTietDonHang> list = new ArrayList<>();
            list.add(chiTietDonHang);

            map.addAttribute("tongSach", tongSach(list));
            map.addAttribute("tinhTien", tinhTien(list));
            map.addAttribute("cart", list);
        } else {
            List<ChiTietDonHang> list = (List<ChiTietDonHang>) map.getAttribute("cart");
            boolean flag = false;
            for (ChiTietDonHang chiTietDonHang : list){
                if (chiTietDonHang.getSach().getId() == sach.getId()){
                    chiTietDonHang.setSoLuong(chiTietDonHang.getSoLuong() + 1);
                    flag = true;
                }
            }
            if (!flag){
                ChiTietDonHang chiTietDonHang = new ChiTietDonHang();
                chiTietDonHang.setSoLuong(1);
                chiTietDonHang.setSach(sach);
                chiTietDonHang.setGiaBan(sach.getGia());

                list.add(chiTietDonHang);
            }

            map.addAttribute("tongSach", tongSach(list));
            map.addAttribute("tinhTien", tinhTien(list));
            map.addAttribute("cart", list);
        }
        return "other/cart";
    }


    @GetMapping("/gio-hang")
    public String gioHang(){
        return "other/cart";
    }

    @GetMapping("/xoa/{id}")
    public String xoaSp(@PathVariable int id,
            ModelMap map){
        List<ChiTietDonHang> list = (List<ChiTietDonHang>) map.getAttribute("cart");
        for (ChiTietDonHang chiTietDonHang : list){
            if (chiTietDonHang.getSach().getId() == id){
                list.remove(chiTietDonHang);
                break;
            }
        }

        map.addAttribute("tongSach", tongSach(list));
        map.addAttribute("tinhTien", tinhTien(list));
        map.addAttribute("cart", list);
        return "other/cart";
    }

    @GetMapping("/user/thanh-toan")
    public String thanhToan(ModelMap map){
        map.addAttribute("donHang", new DonHang());
        return "user/checkout";
    }

    @PostMapping("/user/dat-hang")
    public String datHang(@ModelAttribute DonHang donHang,
                          ModelMap map){
        List<ChiTietDonHang> list = (List<ChiTietDonHang>) map.getAttribute("cart");
        donHang.setTongTien((long) tinhTien(list));
        long millis=System.currentTimeMillis();
        donHang.setNgayDatHang(new Date(millis));
        donHang.setTrangThai(2);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userDAO.getUserByUsername(((UserDetails) principal).getUsername());
        donHang.setIdUser(user.getId());
        donHang.setListCTDonHang(list);

        donHangService.DatHang(donHang);
        return "index";
    }

    public static double tinhTien(List<ChiTietDonHang> list){
        double total = 0;
        for (ChiTietDonHang chiTietDonHang: list){
            total += chiTietDonHang.getGiaBan()*chiTietDonHang.getSoLuong();
        }
        return total;
    }

    public static int tongSach(List<ChiTietDonHang> list){
        int total = 0;
        for (ChiTietDonHang chiTietDonHang : list){
            total += chiTietDonHang.getSoLuong();
        }
        return total;
    }
}
