package com.nvn41091.controller.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class HomeException {

    @ExceptionHandler(value = NoHandlerFoundException.class)
    public String pageNotFound(){
        return "redirect:/404";
    }
}
