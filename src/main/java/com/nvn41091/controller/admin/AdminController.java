package com.nvn41091.controller.admin;

import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.Sach;
import com.nvn41091.model.SachTemp;
import com.nvn41091.model.User;
import com.nvn41091.service.DonHangService;
import com.nvn41091.service.SachService;
import com.nvn41091.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private SachService sachService;

    @Autowired
    private DonHangService donHangService;

    @GetMapping(value = {"/"})
    public String homeController(ModelMap map) {
        map.addAttribute("soLuong", userDAO.soLuongBanTrongTuan());
        map.addAttribute("tongTienBan", userDAO.tongTienBanTrongTuan());
        map.addAttribute("user", userDAO.countUser());
        map.addAttribute("xuLy", userDAO.donHangCanXuLy());
        map.addAttribute("items", donHangService.getAllDon());
        return "admin/index";
    }

    @GetMapping(value = {"/user"})
    public String quanLyNguoiDung(ModelMap map) {
        List<User> list = userService.getAllUser();
        map.addAttribute("items", list);
        return "admin/user";
    }

    @GetMapping("/lock/{id}")
    public String lockUser(@PathVariable("id") int id, ModelMap map) {
        userService.lockUser(id);
        List<User> list = userService.getAllUser();
        map.addAttribute("items", list);
        return "admin/user";
    }

    @GetMapping("/unlock/{id}")
    public String unlockUser(@PathVariable("id") int id, ModelMap map) {
        userService.unlockUser(id);
        List<User> list = userService.getAllUser();
        map.addAttribute("items", list);
        return "admin/user";
    }

    @GetMapping("/book")
    public String getBook(ModelMap map) {
        map.addAttribute("items", sachService.getAllSach());
        map.addAttribute("newSach", new SachTemp());
        return "admin/book";
    }

    @GetMapping("/xoaSach/{id}")
    public String deleteSach(@PathVariable("id") int id) {
        sachService.deleteSach(id);
        return "redirect:/admin/book";
    }

    @PostMapping("/changeSach")
    public String changeSach(@RequestParam("tenSach") String tenSach,
                             @RequestParam("xuatBan") Date xuatBan,
                             @RequestParam("nhaXuatBan") String nhaXuatBan,
                             @RequestParam("nhaPhatHanh") String nhaPhatHanh,
                             @RequestParam("dangBia") String dangBia,
                             @RequestParam("kichThuoc") String kichThuoc,
                             @RequestParam("soTrang") int soTrang,
                             @RequestParam("khoiLuong") String khoiLuong,
                             @RequestParam("gia") int gia,
                             @RequestParam("gioiThieu") String gioiThieu,
                             @RequestParam("id") int id) {
        Sach sach = new Sach();
        sach.setId(id);
        sach.setTenSach(tenSach);
        sach.setXuatBan(xuatBan);
        sach.setNhaPhatHanh(nhaPhatHanh);
        sach.setNhaXuatBan(nhaXuatBan);
        sach.setDangBia(dangBia);
        sach.setKichThuoc(kichThuoc);
        sach.setSoTrang(soTrang);
        sach.setKhoiLuong(khoiLuong);
        sach.setKichThuoc(kichThuoc);
        sach.setGia(gia);
        sach.setGioiThieu(gioiThieu);
        sachService.updateSach(sach);
        return "redirect:/admin/book";
    }

    @Transactional
    @PostMapping("/themSach")
    public String themSach(@ModelAttribute("newSach") SachTemp sach){
        try {
            File file = new File("D:/MyProject/bookstore/src/main/resources/static/images/product/" + sach.getAnhTemp().getOriginalFilename() );
            FileOutputStream fileOutputStream;
            fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(sach.getAnhTemp().getBytes());
            fileOutputStream.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        sachService.addSach(sach);
        return "redirect:/admin/book";
    }

    @GetMapping("/donHang")
    public String getDonHang(ModelMap map){
        map.addAttribute("items", donHangService.getAllDon());
        return "admin/donhang";
    }

    @GetMapping("/reDon/{id}")
    public String reDon(@PathVariable("id") int id){
        donHangService.changeTrangThai(id, 1);
        return "redirect:/admin/donHang";
    }

    @GetMapping("/unDon/{id}")
    public String unDon(@PathVariable("id") int id){
        donHangService.changeTrangThai(id, 2);
        return "redirect:/admin/donHang";
    }

    @GetMapping("/delDon/{id}")
    public String delDon(@PathVariable("id") int id){
        donHangService.changeTrangThai(id, 0);
        return "redirect:/admin/donHang";
    }

}
