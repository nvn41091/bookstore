package com.nvn41091.controller.admin;

import com.nvn41091.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class RestControl {

    @Autowired
    private UserDAO userDAO;

    @GetMapping("/bieuDo")
    public List<Integer> getBieuDo(){
        try {
            return userDAO.bieuDo();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
