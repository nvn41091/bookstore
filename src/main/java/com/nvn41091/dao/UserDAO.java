package com.nvn41091.dao;

import com.nvn41091.model.User;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserDAO {
    
    public List<User> getAllUser();
    
    public List<User> getUserByPages(int page);
    
    public User getUserByID(int id);
    
    public boolean setPremission(int id, String premission);
    
    public boolean editUser(User user);
    
    public boolean addUser(User user);
    
    public boolean checkExistUser(int id);
    
    public User getUserByUsername(String username);
    
    public int countUser();

    public void lockUser(int id);

    public void unlockUser(int id);

    int soLuongBanTrongTuan();

    long tongTienBanTrongTuan();

    int donHangCanXuLy();

    List<Integer> bieuDo();
}
