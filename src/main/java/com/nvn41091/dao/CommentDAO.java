package com.nvn41091.dao;

import com.nvn41091.model.Comment;
import java.util.List;

public interface CommentDAO {
    
    public Comment getCommentByID(int id);
    
    public List<Comment> getCommentBySach(int idSach);
    
    public boolean addComment(Comment comment);
    
    public boolean editComment(Comment comment);
    
    public boolean deleteComment(int id);
}
