package com.nvn41091.dao;

import com.nvn41091.model.YeuThich;

import java.util.List;

public interface YeuThichDAO {

    List<YeuThich> getWishlist(int id);

    void addWishList(int id_user, int id_sach);

    void deleteWishList(int id_user, int id_sach);

}
