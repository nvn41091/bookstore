package com.nvn41091.dao;

import com.nvn41091.model.ChiTietDonHang;
import java.util.List;

public interface ChiTietDonHangDAO {
    
    List<ChiTietDonHang> getChiTiet(int idDonHang);

    void insert(ChiTietDonHang chiTietDonHang, int id);

    List<ChiTietDonHang> getListByUser(int userId);
}
