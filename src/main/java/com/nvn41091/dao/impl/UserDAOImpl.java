package com.nvn41091.dao.impl;

import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.CustomUserDetails;
import com.nvn41091.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.object.SqlQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private class UserMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {
            User user = new User();
            user.setId(rs.getInt("id_user"));
            user.setUsername(rs.getString("username"));
            user.setPassword(rs.getString("password"));
            user.setHoTen(rs.getString("ho_ten"));
            user.setNgaySinh(rs.getDate("ngay_sinh"));
            user.setGioiTinh(rs.getInt("gioi_tinh"));
            user.setDiaChi(rs.getString("dia_chi"));
            user.setSoDienThoai(rs.getString("so_dien_thoai"));
            user.setQuyen(rs.getString("quyen"));
            user.setEnable(rs.getInt("enable"));
            return user;
        }

    }

    @Override
    public List<User> getAllUser() {
        String sql = "SELECT * FROM user";
        List<User> list = jdbcTemplate.query(sql, new Object[]{}, new UserMapper());
        return list;
    }

    @Override
    public List<User> getUserByPages(int page) {
        String sql = "SELECT * FROM `user` LIMIT ?,7";
        List<User> list = jdbcTemplate.query(sql, new Object[]{(page - 1) * 7}, new UserMapper());
        return list;
    }

    @Override
    public User getUserByID(int id) {
        String sql = "SELECT * FROM user WHERE id = ?";
        User user = jdbcTemplate.queryForObject(sql, new Object[]{id}, new UserMapper());
        return user;
    }

    @Override
    public boolean setPremission(int id, String premission) {
        String sql = "UPDATE `user` SET quyen = ? WHERE id_user = ?";
        int a = jdbcTemplate.update(sql, premission, id);
        if (a > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean editUser(User user) {
        String sql = "UPDATE `user` SET `password` = ?, ho_ten = ?, dia_chi = ?, gioi_tinh = ?, ngay_sinh = ? "
                + "WHERE id_user = ?";
        int a = jdbcTemplate.update(sql, passwordEncoder.encode(user.getPassword()), user.getHoTen(), user.getDiaChi(), user.getGioiTinh(), user.getNgaySinh());
        if (a > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean addUser(User user) {
        String sql = "INSERT INTO `user`(username ,`password`, ho_ten, ngay_sinh, gioi_tinh, dia_chi, so_dien_thoai, quyen, enable) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, 'ROLE_USER', 1)";
        int a = jdbcTemplate.update(sql, user.getUsername(), passwordEncoder.encode(user.getPassword()), user.getHoTen(),
                user.getNgaySinh(), user.getGioiTinh(), user.getDiaChi(), user.getSoDienThoai());
        if (a > 0) {
            return true;
        }
        return false;
    }

    @Override
    public boolean checkExistUser(int id) {
        User user = getUserByID(id);
        if (user.getId() != 0) {
            return true;
        }
        return false;
    }

    @Override
    public User getUserByUsername(String username) {
        String sql = "SELECT * FROM `user` WHERE username = ?";
        User user = jdbcTemplate.queryForObject(sql, new Object[]{username}, new UserMapper());
        return user;
    }

    @Override
    public int countUser() {
        String sql = "SELECT count(*) as sl FROM user";
        int count = jdbcTemplate.queryForObject(sql, Integer.class);
        return count;
    }

    @Override
    public void lockUser(int id) {
        String sql = "UPDATE user set enable = 0 WHERE id_user = ? ";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    @Override
    public void unlockUser(int id) {
        String sql = "UPDATE user set enable = 1 WHERE id_user = ? ";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    @Override
    public int soLuongBanTrongTuan() {
        String sql = "SELECT SUM(so_luong) as sl FROM don_hang\n" +
                "INNER JOIN chi_tiet_don_hang on don_hang.id_hoa_don = chi_tiet_don_hang.id_hoa_don\n" +
                "WHERE ngay_dat_hang BETWEEN '2020-06-08' AND '2020-06-15' AND trang_thai = 1";
        int a = jdbcTemplate.queryForObject(sql, Integer.class);
        return a;
    }

    @Override
    public long tongTienBanTrongTuan() {
        String sql = "SELECT SUM(so_luong*gia_ban) FROM don_hang\n" +
                "INNER JOIN chi_tiet_don_hang on don_hang.id_hoa_don = chi_tiet_don_hang.id_hoa_don\n" +
                "WHERE ngay_dat_hang BETWEEN '2020-06-08' AND '2020-06-15' AND trang_thai = 1";
        long a = jdbcTemplate.queryForObject(sql, Long.class);
        return a;
    }

    @Override
    public int donHangCanXuLy() {
        String sql = "SELECT COUNT(chi_tiet_don_hang.id_hoa_don) FROM don_hang\n" +
                "INNER JOIN chi_tiet_don_hang on don_hang.id_hoa_don = chi_tiet_don_hang.id_hoa_don\n" +
                "WHERE ngay_dat_hang BETWEEN '2020-06-08' AND '2020-06-15' AND trang_thai = 2";
        int a = jdbcTemplate.queryForObject(sql, Integer.class);
        return a;
    }

    @Override
    public List<Integer> bieuDo() {
        String sql = "SELECT SUM(SL) as sl FROM ( SELECT dt, IFNULL(so_luong, '0') sl FROM calendar_table \n" +
                "LEFT JOIN don_hang on calendar_table.dt = don_hang.ngay_dat_hang\n" +
                "left join chi_tiet_don_hang on don_hang.id_hoa_don = chi_tiet_don_hang.id_hoa_don\n" +
                "WHERE calendar_table.dt BETWEEN '2020-06-08' AND '2020-06-15' ) AS BANG1\n" +
                "GROUP BY dt\n" +
                "ORDER BY dt";
        List<Integer> list = jdbcTemplate.query(sql, new Object[]{}, (resultSet, i) -> resultSet.getInt("sl"));

        return list;
    }
}
