package com.nvn41091.dao.impl;

import com.nvn41091.dao.SachDAO;
import com.nvn41091.model.Sach;
import com.nvn41091.model.SachTemp;
import com.nvn41091.model.TacGia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class SachDAOImpl implements SachDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private class SachMapper implements RowMapper<Sach> {

        @Override
        public Sach mapRow(ResultSet rs, int i) throws SQLException {
            Sach sach = new Sach();
            sach.setId(rs.getInt("id_sach"));

            TacGia tacgia = new TacGia();
            tacgia.setId(rs.getInt("id_tac_gia"));
            tacgia.setTenTacGia(rs.getString("ten_tac_gia"));
            tacgia.setNgaySinh(rs.getDate("ngay_sinh"));
            tacgia.setTieuSu(rs.getString("tieu_su"));
            sach.setTacGia(tacgia);

            sach.setLoaiSach(rs.getString("ten_loai_sach"));
            sach.setTenSach(rs.getString("ten_sach"));
            sach.setXuatBan(rs.getDate("xuat_ban"));
            sach.setGioiThieu(rs.getString("gioi_thieu"));
            sach.setRate((float) Math.ceil((rs.getFloat("rate"))));
            sach.setNhaXuatBan(rs.getString("nha_xuat_ban"));
            sach.setNhaPhatHanh(rs.getString("nha_phat_hanh"));
            sach.setDangBia(rs.getString("dang_bia"));
            sach.setKichThuoc(rs.getString("kich_thuoc"));
            sach.setKhoiLuong(rs.getString("khoi_luong"));
            sach.setSoTrang(rs.getInt("so_trang"));
            sach.setGia(rs.getInt("gia"));
            sach.setAnh(rs.getString("anh"));
            return sach;
        }

    }

    private final String mainSql = "SELECT sach.id_sach,sach.gioi_thieu, sach.id_tac_gia, sach.ten_sach, sach.xuat_ban, sach.nha_xuat_ban, sach.gia, Bang1.rate, tac_gia.ten_tac_gia, tac_gia.tieu_su,tac_gia.ngay_sinh, loai_sach.ten_loai_sach, sach.nha_phat_hanh, sach.dang_bia, sach.kich_thuoc, sach.khoi_luong, sach.so_trang, sach.anh\n" +
            "FROM sach\n" +
            "INNER JOIN tac_gia on sach.id_tac_gia = tac_gia.id_tac_gia\n" +
            "INNER JOIN loai_sach on sach.id_loai_sach = loai_sach.id_loai_sach " +
            "LEFT JOIN  ( SELECT id_sach as temp,AVG(rate) as rate FROM rate GROUP BY id_sach ) as Bang1 on Bang1.temp = sach.id_sach\n";

    @Override
    public Sach getBookById(int id) {
        String sql = mainSql + " WHERE sach.id_sach = ? ";

        Sach sach = jdbcTemplate.queryForObject(sql, new Object[]{id}, new SachMapper());

        return sach;
    }

    @Override
    public List<Sach> getBookBypages(int page) {
        String sql = mainSql + "LIMIT ?,7";
        List<Sach> list = jdbcTemplate.query(sql, new Object[]{page - 1}, new SachMapper());
        return list;
    }

    @Override
    public List<Sach> getBookByTacGia(String tacgia, int page) {
        String sql = mainSql + " WHERE tac_gia.ten_tac_gia LIKE ? LIMIT ?,7";

        List<Sach> list = jdbcTemplate.query(sql, new Object[]{tacgia, page - 1}, new SachMapper());

        return list;
    }

    @Override
    public List<Sach> getBookByLoaiSach(String loaiSach, int page) {
        String sql = mainSql + " WHERE loai_sach.ten_loai_sach LIKE ? LIMIT ?,7";

        List<Sach> list = jdbcTemplate.query(sql, new Object[]{loaiSach, page - 1}, new SachMapper());

        return list;
    }

    @Override
    public List<Sach> getBookByBuy() {
        String sql = mainSql + " WHERE sach.id_sach in " +
                "(SELECT id FROM (SELECT chi_tiet_don_hang.id_sach as id, COUNT(chi_tiet_don_hang.id_hoa_don) as count FROM chi_tiet_don_hang ORDER BY count desc limit 0,7) as bang1)";

        List<Sach> list = jdbcTemplate.query(sql, new Object[]{}, new SachMapper());

        return list;
    }

    @Override
    public List<Sach> getBookByPrice(int min, int max, int page) {
        String sql = mainSql + "WHERE gia BETWEEN ? AND ? LIMIT ?,7";

        List<Sach> list = jdbcTemplate.query(sql, new Object[]{min, max, page - 1}, new SachMapper());

        return list;
    }

    @Override
    public List<Sach> getNewBook() {
        String sql = mainSql + " ORDER BY id_sach DESC LIMIT 0,9";

        List<Sach> list = jdbcTemplate.query(sql, new Object[]{}, new SachMapper());

        return list;
    }

    @Override
    public List<Sach> searchBook(String s) {
        String sql = mainSql + " WHERE sach.ten_sach LIKE " + "'%" + s + "%'";
        List<Sach> list = jdbcTemplate.query(sql, new Object[]{}, new SachMapper());
        return list;
    }

    @Override
    public List<Sach> getListByLoaiSach(int loaiSach) {
        String sql = mainSql + " WHERE loai_sach.id_loai_sach = ?";
        List<Sach> list = jdbcTemplate.query(sql, new Object[]{loaiSach}, new SachMapper());
        return list;
    }

    @Override
    public List<Sach> getAllSach() {
        String sql = mainSql;
        List<Sach> list = jdbcTemplate.query(sql, new Object[]{}, new SachMapper());
        return list;
    }

    @Override
    public void deleteSach(int id) {
        String sql = "DELETE FROM sach WHERE id_sach = ?";
        jdbcTemplate.update(sql, new Object[]{id});
    }

    @Override
    public void updateSach(Sach sach) {
        String sql = "UPDATE `bookstore`.`sach` SET " +
                "`ten_sach` = ?, " +
                "`xuat_ban` = ?, `nha_xuat_ban` = ?, " +
                "`nha_phat_hanh` = ?, " +
                "`dang_bia` = ?, " +
                "`kich_thuoc` = ?, " +
                "`khoi_luong` = ?, " +
                "`so_trang` = ?, " +
                "`gia` = ?, " +
                "`gioi_thieu` = ? " +
                "WHERE `id_sach` = ?";
        jdbcTemplate.update(sql, new Object[]{sach.getTenSach(), sach.getXuatBan().toString().split("-")[0], sach.getNhaXuatBan(),
                sach.getNhaPhatHanh(), sach.getDangBia(), sach.getKichThuoc(), sach.getKhoiLuong(), sach.getSoTrang(),
                sach.getGia(), sach.getGioiThieu(), sach.getId()});
    }

    @Override
    public void addSach(SachTemp sach) {
        String sql = "INSERT INTO `bookstore`.`sach`(`id_tac_gia`, `id_loai_sach`, " +
                "`ten_sach`, `xuat_ban`, `nha_xuat_ban`, `nha_phat_hanh`, `dang_bia`, " +
                "`kich_thuoc`, `khoi_luong`, `so_trang`, `gia`, `anh`, `gioi_thieu`) " +
                "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        String sql1 = "INSERT INTO `bookstore`.`tac_gia`( `ten_tac_gia`, `ngay_sinh`, `tieu_su`) " +
                "VALUES ( ?, NULL, NULL);\n";
        final PreparedStatementCreator psc = connection -> {
            final PreparedStatement ps = connection.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, sach.getTacGia().getTenTacGia());
            return ps;
        };

        final KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(psc, keyHolder);

        final int id = keyHolder.getKey().intValue();
        int a = 1;
        try {
            a = Integer.valueOf(sach.getLoaiSach());
        } catch (Exception e) {
            a = 1;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String time = simpleDateFormat.format(sach.getXuatBan());
        System.out.println(time);
        jdbcTemplate.update(sql, new Object[]{id, a , sach.getTenSach(),
                time, sach.getNhaXuatBan(), sach.getNhaPhatHanh(),
                sach.getDangBia(), sach.getKichThuoc(), sach.getKhoiLuong(), sach.getSoTrang(), sach.getGia(),
                sach.getAnhTemp().getOriginalFilename(), sach.getGioiThieu()});
    }

}
