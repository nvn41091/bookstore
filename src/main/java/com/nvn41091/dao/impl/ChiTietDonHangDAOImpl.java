package com.nvn41091.dao.impl;

import com.nvn41091.dao.ChiTietDonHangDAO;
import com.nvn41091.dao.SachDAO;
import com.nvn41091.model.ChiTietDonHang;
import com.nvn41091.model.Sach;

import java.sql.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class ChiTietDonHangDAOImpl implements ChiTietDonHangDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @Autowired
    private SachDAO sachDAO;

    private class ChiTietDonHangMapper implements RowMapper<ChiTietDonHang> {

        @Override
        public ChiTietDonHang mapRow(ResultSet rs, int i) throws SQLException {
            ChiTietDonHang chiTietDonHang = new ChiTietDonHang();
            
            chiTietDonHang.setSach(sachDAO.getBookById(rs.getInt("id_sach")));
            chiTietDonHang.setGiaBan(rs.getInt("gia_ban"));
            chiTietDonHang.setSoLuong(rs.getInt("so_luong"));
            
            return chiTietDonHang;
        }

    }

    private final String mainSQL = "SELECT * FROM chi_tiet_don_hang ";
    
    @Override
    public List<ChiTietDonHang> getChiTiet(int idDonHang) {
        String sql = mainSQL + "WHERE id_hoa_don = ?";
        
        List<ChiTietDonHang> list = jdbcTemplate.query(sql, new Object[]{idDonHang}, new ChiTietDonHangMapper());
        
        return list;
    }

    @Transactional
    @Override
    public void insert(ChiTietDonHang chiTietDonHang, int id) {
        String sql = "INSERT INTO `chi_tiet_don_hang`(`id_hoa_don`, `id_sach`, `gia_ban`, `so_luong`) VALUES (?, ?, ?, ?)";

        jdbcTemplate.update(sql, id, chiTietDonHang.getSach().getId(), chiTietDonHang.getGiaBan(), chiTietDonHang.getSoLuong());
    }

    @Override
    public List<ChiTietDonHang> getListByUser(int userId) {
        String sql = "SELECT chi_tiet_don_hang.id_sach, chi_tiet_don_hang.gia_ban, chi_tiet_don_hang.so_luong FROM don_hang \n" +
                "INNER JOIN chi_tiet_don_hang ON don_hang.id_hoa_don = chi_tiet_don_hang.id_hoa_don WHERE don_hang.id_user = ?";
        final PreparedStatementCreator psc = connection -> {
            final PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, userId);
            return ps;
        };

        List<ChiTietDonHang> list = jdbcTemplate.query(psc, new ChiTietDonHangMapper());
        return list;
    }

}
