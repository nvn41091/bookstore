package com.nvn41091.dao.impl;

import com.nvn41091.dao.YeuThichDAO;
import com.nvn41091.model.YeuThich;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
public class YeuThichDAOImpl implements YeuThichDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public class YeuThichMapper implements RowMapper<YeuThich>{

        @Override
        public YeuThich mapRow(ResultSet resultSet, int i) throws SQLException {
            YeuThich yeuThich = new YeuThich();
            yeuThich.setIdSach(resultSet.getInt("id_sach"));
            return yeuThich;
        }
    }

    private final String mainSql = "SELECT id_sach FROM yeu_thich ";

    @Override
    public List<YeuThich> getWishlist(int id) {
        String sql = mainSql + " WHERE id_user = ?";
        List<YeuThich> list = jdbcTemplate.query(sql, new Object[]{id}, new YeuThichMapper());
        return list;
    }

    @Override
    public void addWishList(int id_user, int id_sach) {
        String sql = "INSERT INTO yeu_thich(id_user, id_sach) VALUES (?,?)";
        jdbcTemplate.update(sql, new Object[]{id_user, id_sach});
    }

    @Override
    public void deleteWishList(int id_user, int id_sach) {
        String sql = "DELETE FROM yeu_thich WHERE id_user = ? AND id_sach = ?";
        jdbcTemplate.update(sql, new Object[]{id_user, id_sach});
    }

}
