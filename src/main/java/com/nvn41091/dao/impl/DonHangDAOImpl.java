package com.nvn41091.dao.impl;

import com.nvn41091.dao.ChiTietDonHangDAO;
import com.nvn41091.dao.DonHangDAO;
import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.ChiTietDonHang;
import com.nvn41091.model.DonHang;

import java.sql.*;
import java.util.List;

import com.nvn41091.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class DonHangDAOImpl implements DonHangDAO {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    ChiTietDonHangDAO chiTietDonHangDAO;

    private final String mainSql = "SELECT * FROM don_hang";

    private class DonHangMapper implements RowMapper<DonHang> {

        @Override
        public DonHang mapRow(ResultSet rs, int i) throws SQLException {
            DonHang donHang = new DonHang();
            donHang.setId(rs.getInt("id_hoa_don"));
            donHang.setIdUser(rs.getInt("id_user"));
            donHang.setTrangThai(rs.getInt("trang_thai"));
            donHang.setNgayDatHang(rs.getDate("ngay_dat_hang"));
            donHang.setNgayGiaoHang(rs.getDate("ngay_giao_hang"));
            donHang.setNoiGiaoHang(rs.getString("noi_giao_hang"));
            donHang.setSoDienThoai(rs.getString("so_dien_thoai"));
            donHang.setEmail(rs.getString("email"));
            donHang.setHo(rs.getString("ho"));
            donHang.setTen(rs.getString("ten"));
            return donHang;
        }

    }

    @Override
    public DonHang getDonByID(int id) {
        String sql = mainSql + "WHERE id_hoa_don = ?";

        DonHang donHang = jdbcTemplate.queryForObject(sql, new Object[]{id}, new DonHangMapper());

        return donHang;
    }

    @Override
    public List<DonHang> getDonByUsernameID(int idUser, int page) {
        String sql = mainSql + "WHERE id_user = ? LIMIT ?,7";

        List<DonHang> list = jdbcTemplate.query(sql, new Object[]{idUser, page - 1}, new DonHangMapper());

        return list;
    }

    @Override
    public List<DonHang> getDonByTrangThai(int trangThai, int page) {
        String sql = mainSql + "WHERE trang_thai = ? LIMIT ?,7";

        List<DonHang> list = jdbcTemplate.query(sql, new Object[]{trangThai, page - 1}, new DonHangMapper());

        return list;
    }

    @Transactional
    @Override
    public void datHang(DonHang donHang) {
        String sql = "INSERT INTO don_hang(id_user, trang_thai, ngay_dat_hang, ngay_giao_hang, noi_giao_hang, so_dien_thoai, ho, ten, email) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

        final PreparedStatementCreator psc = connection -> {
            final PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, donHang.getIdUser());
            ps.setInt(2, donHang.getTrangThai());
            ps.setString(3, donHang.getNgayDatHang().toString());
            ps.setString(4, null);
            ps.setString(5, donHang.getNoiGiaoHang());
            ps.setString(6, donHang.getSoDienThoai());
            ps.setString(7, donHang.getHo());
            ps.setString(8, donHang.getTen());
            ps.setString(9, donHang.getEmail());
            return ps;
        };

        final KeyHolder holder = new GeneratedKeyHolder();

        jdbcTemplate.update(psc, holder);

        final int id = holder.getKey().intValue();

        for (ChiTietDonHang chiTietDonHang : donHang.getListCTDonHang()){
            chiTietDonHangDAO.insert(chiTietDonHang, id);
        }

    }

    @Override
    public List<DonHang> getAllDon() {
        String sql = mainSql;
        List<DonHang> list = jdbcTemplate.query(sql , new Object[]{}, new DonHangMapper());
        for (DonHang donHang : list){
            donHang.setListCTDonHang(chiTietDonHangDAO.getChiTiet(donHang.getId()));
        }
        return list;
    }

    @Override
    public void changeTrangThai(int idDon, int trangThai) {
        String sql = "Update don_hang SET trang_thai = ? WHERE id_hoa_don = ?";
        jdbcTemplate.update(sql, new Object[]{trangThai, idDon});
    }

}
