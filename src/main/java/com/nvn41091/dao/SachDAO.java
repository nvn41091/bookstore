package com.nvn41091.dao;

import com.nvn41091.model.Sach;
import com.nvn41091.model.SachTemp;

import java.util.List;

public interface SachDAO {

    public Sach getBookById(int id);

    public List<Sach> getBookBypages(int page);

    public List<Sach> getBookByTacGia(String tacgia, int page);

    List<Sach> getBookByLoaiSach(String loaiSach, int page);

    List<Sach> getBookByBuy();

    public List<Sach> getBookByPrice(int min, int max, int page);

    public List<Sach> getNewBook();

    List<Sach> searchBook(String s);

    List<Sach> getListByLoaiSach(int loaiSach);

    public List<Sach> getAllSach();

    void deleteSach(int id);

    void updateSach(Sach sach);

    void addSach(SachTemp sach);

}
