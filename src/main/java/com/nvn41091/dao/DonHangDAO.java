package com.nvn41091.dao;

import com.nvn41091.model.DonHang;
import java.util.List;

public interface DonHangDAO {
    
    public DonHang getDonByID(int id);
    
    public List<DonHang> getDonByUsernameID(int idUser, int page);
    
    public List<DonHang> getDonByTrangThai(int trangThai, int page);

    public void datHang(DonHang donHang);

    List<DonHang> getAllDon();

    void changeTrangThai(int idDon, int trangThai);
    
}
