package com.nvn41091.service;

import com.nvn41091.model.DonHang;

import java.util.List;

public interface DonHangService {

    public void DatHang(DonHang donHang);

    List<DonHang> getAllDon();

    void changeTrangThai(int idDon, int trangThai);
}
