package com.nvn41091.service;

import com.nvn41091.model.YeuThich;

import java.util.List;

public interface YeuThichService {

    List<YeuThich> getListYeuThich(int id);

    void addYeuThich(int idUser, int idSach);

    void deleteYeuThich(int idUser, int idSach);
}
