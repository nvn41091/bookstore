package com.nvn41091.service;

import com.nvn41091.model.User;
import java.util.List;

public interface UserService {

    public List<User> getAllUser();

    public List<User> getUserByPages(int page);

    public User getUserByID(int id);

    public boolean setPremission(int id, String premission);

    public boolean editUser(User user);

    public boolean addUser(User user);

    public User getUserByUsername(String username);

    public void lockUser(int id);

    public void unlockUser(int id);

}
