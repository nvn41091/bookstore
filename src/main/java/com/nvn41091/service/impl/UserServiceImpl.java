package com.nvn41091.service.impl;

import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.User;
import com.nvn41091.service.UserService;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    
    @Autowired
    private UserDAO userDAO;

    @Override
    public List<User> getAllUser() {
        return userDAO.getAllUser();
    }

    @Override
    public List<User> getUserByPages(int page) {
        try {
            return userDAO.getUserByPages(page);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public User getUserByID(int id) {
        try {
            User user = userDAO.getUserByID(id);
            return user;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean setPremission(int id, String premission) {
        try {
            boolean mess = userDAO.setPremission(id, premission);
            return mess;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean editUser(User user) {
        try {
            boolean mess = userDAO.editUser(user);
            return mess;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean addUser(User user) {
        try {
            boolean mess = userDAO.addUser(user);
            return mess;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
    }

    @Override
    public User getUserByUsername(String username) {
        try {
            User user = userDAO.getUserByUsername(username);
            return user;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    public void lockUser(int id) {
        try {
            userDAO.lockUser(id);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void unlockUser(int id) {
        try {
            userDAO.unlockUser(id);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}
