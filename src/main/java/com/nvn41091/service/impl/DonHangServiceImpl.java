package com.nvn41091.service.impl;

import com.nvn41091.dao.DonHangDAO;
import com.nvn41091.model.DonHang;
import com.nvn41091.service.DonHangService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DonHangServiceImpl implements DonHangService {

    @Autowired
    private DonHangDAO donHangDAO;

    @Override
    public void DatHang(DonHang donHang) {
        try {
            donHangDAO.datHang(donHang);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public List<DonHang> getAllDon() {
        try {
            return donHangDAO.getAllDon();
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void changeTrangThai(int idDon, int trangThai) {
        try {
            donHangDAO.changeTrangThai(idDon, trangThai);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
