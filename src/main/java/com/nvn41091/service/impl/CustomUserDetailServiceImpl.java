package com.nvn41091.service.impl;

import com.nvn41091.dao.UserDAO;
import com.nvn41091.model.CustomUserDetails;
import com.nvn41091.model.User;
import com.nvn41091.service.UserService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailServiceImpl implements UserDetailsService{
    
    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User " + username + " was not found in the database");
        }
        
        CustomUserDetails userDetails = new CustomUserDetails();
        userDetails.setUsername(user.getUsername());
        userDetails.setPassword(user.getPassword());
        if (user.getEnable() == 1) {
            userDetails.setEnable(true);
        } else {
            userDetails.setEnable(false);
        }
        List<GrantedAuthority> listGrant = new ArrayList<>();
        listGrant.add(new SimpleGrantedAuthority(user.getQuyen()));
        if (user.getQuyen().equals("ROLE_ADMIN")) {
            listGrant.add(new SimpleGrantedAuthority("ROLE_USER"));
        }
        userDetails.setAuth(listGrant);
        return userDetails;
    }
    
}
