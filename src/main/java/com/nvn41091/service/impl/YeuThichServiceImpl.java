package com.nvn41091.service.impl;

import com.nvn41091.dao.YeuThichDAO;
import com.nvn41091.model.YeuThich;
import com.nvn41091.service.YeuThichService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YeuThichServiceImpl implements YeuThichService {

    @Autowired
    YeuThichDAO yeuThichDAO;


    @Override
    public List<YeuThich> getListYeuThich(int id) {
        try {
            return yeuThichDAO.getWishlist(id);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void addYeuThich(int idUser, int idSach) {
        try {
            yeuThichDAO.addWishList(idUser, idSach);
        } catch (Exception e) {
        }
    }

    @Override
    public void deleteYeuThich(int idUser, int idSach) {
        try {
            yeuThichDAO.deleteWishList(idUser, idSach);
        } catch (Exception e) {
        }
    }
}
