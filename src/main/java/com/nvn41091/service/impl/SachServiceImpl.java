package com.nvn41091.service.impl;

import com.nvn41091.dao.SachDAO;
import com.nvn41091.model.Sach;
import com.nvn41091.model.SachTemp;
import com.nvn41091.service.SachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SachServiceImpl implements SachService {

    @Autowired
    SachDAO sachDAO;

    @Override
    public Sach getBookById(int id) {
        try {
            return sachDAO.getBookById(id);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getAllSach() {
        try {
            return sachDAO.getAllSach();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getBookBypages(int page) {
        try{
            return sachDAO.getBookBypages(page);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getBookByTacGia(String tacgia, int page) {
        try {
            return sachDAO.getBookByTacGia(tacgia, page);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getBookByLoaiSach(String loaiSach, int page) {
        try {
            return sachDAO.getBookByLoaiSach(loaiSach, page);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getBookByBuy() {
        try {
            return sachDAO.getBookByBuy();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getBookByPrice(int min, int max, int page) {
        try {
            return sachDAO.getBookByPrice(min, max, page);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> getNewBook() {
        try {
            return sachDAO.getNewBook();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public List<Sach> searchSach(String s) {
        try {
            return sachDAO.searchBook(s);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<Sach> getListByLoaiSach(int loaiSach) {
        try {
            return sachDAO.getListByLoaiSach(loaiSach);
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public void deleteSach(int id) {
        try {
            sachDAO.deleteSach(id);
        } catch (Exception e){
        }
    }

    @Override
    public void updateSach(Sach sach) {
        try {
            sachDAO.updateSach(sach);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void addSach(SachTemp sach) {
        try{
            sachDAO.addSach(sach);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
