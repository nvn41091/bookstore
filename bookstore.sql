/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : bookstore

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 26/05/2020 22:31:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for chi_tiet_don_hang
-- ----------------------------
DROP TABLE IF EXISTS `chi_tiet_don_hang`;
CREATE TABLE `chi_tiet_don_hang`  (
  `id_hoa_don` int(0) UNSIGNED NOT NULL COMMENT 'id đơn hàng',
  `id_sach` int(0) UNSIGNED NOT NULL COMMENT 'id sách mua',
  `gia_ban` int(0) NOT NULL COMMENT 'giá bán',
  `so_luong` int(0) NOT NULL COMMENT 'số lượng',
  PRIMARY KEY (`id_hoa_don`, `id_sach`) USING BTREE,
  INDEX `FK_chitiet_idsach`(`id_sach`) USING BTREE,
  CONSTRAINT `FK_chitiet_iddon` FOREIGN KEY (`id_hoa_don`) REFERENCES `don_hang` (`id_hoa_don`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_chitiet_idsach` FOREIGN KEY (`id_sach`) REFERENCES `sach` (`id_sach`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of chi_tiet_don_hang
-- ----------------------------

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `id_comment` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id comment',
  `id_user` int(0) UNSIGNED NOT NULL COMMENT 'id user comment',
  `id_sach` int(0) UNSIGNED NOT NULL COMMENT 'id sách comment',
  `noi_dung` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nội dung comment',
  `ngay_comment` date NOT NULL COMMENT 'ngày comment',
  PRIMARY KEY (`id_comment`, `id_user`, `id_sach`) USING BTREE,
  INDEX `FK_sach`(`id_sach`) USING BTREE,
  INDEX `FK_user`(`id_user`) USING BTREE,
  CONSTRAINT `FK_sach` FOREIGN KEY (`id_sach`) REFERENCES `sach` (`id_sach`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of comment
-- ----------------------------

-- ----------------------------
-- Table structure for don_hang
-- ----------------------------
DROP TABLE IF EXISTS `don_hang`;
CREATE TABLE `don_hang`  (
  `id_hoa_don` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id hóa đơn',
  `id_user` int(0) UNSIGNED NOT NULL COMMENT 'id user',
  `trang_thai` int(0) NOT NULL COMMENT 'trạng thái đơn hàng',
  `ngay_dat_hang` date NOT NULL COMMENT 'ngày đặt hàng',
  `ngay_giao_hang` date NULL DEFAULT NULL COMMENT 'ngày giao hàng',
  `noi_giao_hang` varchar(1000) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'nơi giao hàng',
  `so_dien_thoai` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'số điện thoại',
  `tong_tien` bigint(0) NOT NULL COMMENT 'tổng tiền',
  PRIMARY KEY (`id_hoa_don`, `id_user`) USING BTREE,
  INDEX `FK_giaohang_user`(`id_user`) USING BTREE,
  INDEX `id_hoa_don`(`id_hoa_don`) USING BTREE,
  CONSTRAINT `FK_giaohang_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of don_hang
-- ----------------------------

-- ----------------------------
-- Table structure for loai_sach
-- ----------------------------
DROP TABLE IF EXISTS `loai_sach`;
CREATE TABLE `loai_sach`  (
  `id_loai_sach` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID loại sách',
  `id_the_loai` int(0) UNSIGNED NOT NULL,
  `ten_loai_sach` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Tên loại sách',
  PRIMARY KEY (`id_loai_sach`) USING BTREE,
  INDEX `id_the_loai`(`id_the_loai`) USING BTREE,
  CONSTRAINT `loai_sach_ibfk_1` FOREIGN KEY (`id_the_loai`) REFERENCES `the_loai` (`id_the_loai`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of loai_sach
-- ----------------------------
INSERT INTO `loai_sach` VALUES (1, 1, 'Khác');

-- ----------------------------
-- Table structure for persistent_logins
-- ----------------------------
DROP TABLE IF EXISTS `persistent_logins`;
CREATE TABLE `persistent_logins`  (
  `username` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `series` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_used` timestamp(0) NOT NULL,
  PRIMARY KEY (`series`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of persistent_logins
-- ----------------------------
INSERT INTO `persistent_logins` VALUES ('ngoc41099', 'Vqsa3mBwGg1R+M1JnFaDOw==', 'KeJoUgL8dw3m3As+MB04Mw==', '2020-05-04 17:38:24');

-- ----------------------------
-- Table structure for rate
-- ----------------------------
DROP TABLE IF EXISTS `rate`;
CREATE TABLE `rate`  (
  `id_sach` int(0) UNSIGNED NOT NULL COMMENT 'id sách',
  `id_user` int(0) UNSIGNED NOT NULL COMMENT 'id user',
  `rate` int(0) NOT NULL COMMENT 'điểm rate',
  PRIMARY KEY (`id_sach`, `id_user`) USING BTREE,
  INDEX `FK_rate_user`(`id_user`) USING BTREE,
  CONSTRAINT `FK_rate_sach` FOREIGN KEY (`id_sach`) REFERENCES `sach` (`id_sach`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_rate_user` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of rate
-- ----------------------------
INSERT INTO `rate` VALUES (1, 1, 3);
INSERT INTO `rate` VALUES (1, 2, 4);

-- ----------------------------
-- Table structure for sach
-- ----------------------------
DROP TABLE IF EXISTS `sach`;
CREATE TABLE `sach`  (
  `id_sach` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id sách',
  `id_tac_gia` int(0) UNSIGNED NOT NULL COMMENT 'id tác giả',
  `id_loai_sach` int(0) UNSIGNED NOT NULL COMMENT 'id loại sách',
  `ten_sach` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên sách',
  `xuat_ban` year NULL DEFAULT NULL COMMENT 'năm xuất bản',
  `nha_xuat_ban` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'nhà xuất bản',
  `nha_phat_hanh` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'nhà phát hành',
  `dang_bia` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'dạng bìa sách',
  `kich_thuoc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'kích thước',
  `khoi_luong` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'khối lượng sách',
  `so_trang` int(0) NULL DEFAULT NULL COMMENT 'số trang',
  `gia` int(0) NOT NULL COMMENT 'giá bán',
  PRIMARY KEY (`id_sach`, `id_tac_gia`, `id_loai_sach`) USING BTREE,
  INDEX `fk_loai_sach`(`id_loai_sach`) USING BTREE,
  INDEX `fk_tac_gia`(`id_tac_gia`) USING BTREE,
  INDEX `id_sach`(`id_sach`) USING BTREE,
  INDEX `id_sach_2`(`id_sach`) USING BTREE,
  INDEX `id_sach_3`(`id_sach`) USING BTREE,
  INDEX `id_sach_4`(`id_sach`) USING BTREE,
  CONSTRAINT `fk_loai_sach` FOREIGN KEY (`id_loai_sach`) REFERENCES `loai_sach` (`id_loai_sach`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_tac_gia` FOREIGN KEY (`id_tac_gia`) REFERENCES `tac_gia` (`id_tac_gia`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sach
-- ----------------------------
INSERT INTO `sach` VALUES (1, 1, 1, 'Để Mỗi Đứa Trẻ Là Một Thiên Tài', 2011, 'Phụ Nữ', 'Đinh Tị', 'Bìa mềm', '13x20.5 cm', '300 gram', 272, 70000);

-- ----------------------------
-- Table structure for tac_gia
-- ----------------------------
DROP TABLE IF EXISTS `tac_gia`;
CREATE TABLE `tac_gia`  (
  `id_tac_gia` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id tác giả',
  `ten_tac_gia` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'tên tác giả',
  `ngay_sinh` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'ngày tháng năm sinh',
  `tieu_su` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'Tiểu sử',
  PRIMARY KEY (`id_tac_gia`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tac_gia
-- ----------------------------
INSERT INTO `tac_gia` VALUES (1, 'Minh Phượng', NULL, NULL);

-- ----------------------------
-- Table structure for the_loai
-- ----------------------------
DROP TABLE IF EXISTS `the_loai`;
CREATE TABLE `the_loai`  (
  `id_the_loai` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id thể loại sách',
  `ten_the_loai` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_the_loai`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of the_loai
-- ----------------------------
INSERT INTO `the_loai` VALUES (1, 'Khác');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id user',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'username',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'password',
  `ho_ten` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'họ và tên',
  `ngay_sinh` date NULL DEFAULT NULL COMMENT 'ngày sinh',
  `gioi_tinh` int(0) NULL DEFAULT NULL COMMENT 'giới tính',
  `dia_chi` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'địa chỉ',
  `so_dien_thoai` varchar(25) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT 'số điện thoại',
  `quyen` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'quyền truy cập',
  `enable` tinyint(1) NOT NULL COMMENT 'trạng thái',
  PRIMARY KEY (`id_user`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'ngoc41099', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 'Ngoc Nguyen', '1999-10-04', 1, 'Hà Nội', '0387641099', 'ROLE_ADMIN', 1);
INSERT INTO `user` VALUES (2, 'user1', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu', 'User 1', '1998-05-25', 0, 'Bách Khoa', '0123456789', 'ROLE_USER', 1);

-- ----------------------------
-- Triggers structure for table chi_tiet_don_hang
-- ----------------------------
DROP TRIGGER IF EXISTS `edit_don_hang`;
delimiter ;;
CREATE TRIGGER `edit_don_hang` AFTER UPDATE ON `chi_tiet_don_hang` FOR EACH ROW UPDATE don_hang SET don_hang.tong_tien = ( SELECT SUM(gia_ban*so_luong) as total FROM chi_tiet_don_hang 
																							WHERE chi_tiet_don_hang.id_hoa_don = OLD.id_hoa_don )
	WHERE don_hang.id_hoa_don = OLD.id_hoa_don
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
